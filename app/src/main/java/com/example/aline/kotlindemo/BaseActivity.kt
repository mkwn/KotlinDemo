package com.example.aline.kotlindemo

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.telephony.TelephonyManager
import android.widget.Toast
import com.example.aline.kotlindemo.utils.ActivitiesManager

open class BaseActivity : AppCompatActivity() {

    protected lateinit var mContext: Context
    protected lateinit var mActivity: Activity
    protected var isVisiable: Boolean = false
    protected var isDestory: Boolean = false
    private var imie: String? = null
    private val ASKFOEDVICES: Int = 1


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_base)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
        mActivity = this
        mContext = this
        isDestory = false
        ActivitiesManager.getInstance().addActivity(this)
        checkPerssion()

    }

    fun checkPerssion() {
        val pessionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
        if (pessionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_PHONE_STATE), ASKFOEDVICES)
        } else {
            getDevicesImie()
        }
    }

    @SuppressLint("MissingPermission")
    private fun getDevicesImie() {
        val telephonyManager = mContext.getSystemService(
                Context.TELEPHONY_SERVICE) as TelephonyManager
        imie = telephonyManager.deviceId
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == ASKFOEDVICES) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getDevicesImie()
            } else {
                showToast("")
            }

        }

    }

    public fun showToast(s: String) {
        Toast.makeText(mActivity, s, Toast.LENGTH_SHORT).show()
    }

    override fun onResume() {
        super.onResume()
        isVisiable = true
    }

    override fun onPause() {
        super.onPause()
        isVisiable = false
    }

    override fun onDestroy() {
        super.onDestroy()
        isDestory = true
        ActivitiesManager.getInstance().removeActivity(this)
    }

    fun fragmentChange(fragment: Fragment, first: Boolean, id: Int) {
        val transition = supportFragmentManager.beginTransaction()
        if (!first) {
            transition.addToBackStack(null)
        }
        transition.replace(id, fragment)
        transition.commitAllowingStateLoss()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val fragmentManager = supportFragmentManager
        if (fragmentManager != null && fragmentManager.backStackEntryCount > 0) {
            fragmentManager.popBackStackImmediate()
        } else {
            finish()
        }
    }

}

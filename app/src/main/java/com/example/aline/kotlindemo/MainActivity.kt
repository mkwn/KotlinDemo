package com.example.aline.kotlindemo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class MainActivity : AppCompatActivity() {

    val classTwo = ClassTwo("2")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        /**
         * 定义局部变量
         */
        //1.一次赋值（只读）的局部变量：
        val a: Int = 1
        val b = 2
        val c: Int
        c = 8
        //可修改的变量：
        var d = 5
        d = 9

        sum4()

        with(classTwo){
            getSame()
            getSomething("2")
        }
    }

    fun sum(a: Int, b: Int): Int {
        return a + b;
    }

    fun sum1(a: Int, b: Int) = a + b;

    //返回值为空
    fun sum2(a: Int, b: Int): Unit {
        Log.e("sum2", "$a + $b = ${a + b}");
    }

    //返回值为空时可以省略 Uint
    fun sum3(a: Int, b: Int) {
        Log.e("sum3", "$a + $b = ${a + b}");
    }

    //字符模板 -- 实现的功能： 在字符串中进行操作
    fun sum4() {
        var a = 1
        var s1 = "a is $a"
        a = 2
        val s2 = "${s1.replace("is", "was")}, but now a is $a"
        Log.e("sum4", s2)
    }

    //条件表达式
    fun max(a: Int, b: Int): Int {
        if (a > b) {
            return a
        } else {
            return b
        }
    }

    fun max2(a: Int, b: Int) = if (a > b) a else b

    //单纯分支
    fun methodWhen1(x: Int) {
        when (x) {
            1 -> print("x == 1")
            2 -> print("2")
            else -> {
                print(" nor 1 either 2")
            }
        }
    }

    //分之合并
    fun methodWhen2(x: Int) {
        when (x) {
            1, 2 -> print("x == 1")
            else -> {
                print(" nor 1 either 2")
            }
        }
    }

    //分支条件为表达式
    fun methodWhen3(x: Int) {
        when (x) {
            max(1, 2) -> print("max == ")
            else -> {
                print(" nor 1 either 2")
            }
        }
    }

    //分支条件为一个范围值 in   !in
    fun methodWhen4(x: Int) {
        when (x) {
            in 1..3 -> print("between 1 and 3 ")
            !in 4..6 -> print("not 4 to 6")
            else -> {
                print(" not in the range")
            }
        }
    }

    //分支条件为一个判断 is   !is
    fun methodWhen5(x: Any) {
        when (x) {
            is Int -> print("is int")
            is String -> print("is string")
            else -> {
                print("not above")
            }
        }
    }


    fun sum5() {
        val a: Int = 2
        print(a === a)//true  === ： 不仅变量数值相同，同时变量的数据类型也相同

        //Int? 可空标志 表示该变量可以为空
        var boxA: Int? = a
        var boxB: Int? = a
        print(boxA === boxB)//false 值相同数据类型不同
        print(boxA == boxB)//true 数值相同
    }

    fun sum6() {
        //数组
        val array: Array<Int> = arrayOf(1, 3, 3)
        val array2 = arrayOfNulls<Int>(size = 3)//创建一个指定大小的空Array
        val array3: Array<Int?> = arrayOfNulls(3)
        val array4 = Array(3, { i -> (i * i).toString() })
        val array5: IntArray = intArrayOf(34, 32, 32)
        //和 java 不一样，arrays 在 kotlin 中是不可变的。这意味这 kotlin 不允许我们把 Array<String> 转为 Array<Any>
    }

    //字符串模板 -- 包含可求值的代码片段 模板表达式由 $ 开始并包含另一个简单的名称,或者是一个带大括号的表达式:
    fun method7() {
        val a: Int = 10
        val s = "a is $a"
        val b = "a*a = ${a * a}"
    }

    //当空值可能出现时应该明确指出该引用可空。  可空值和可空函数  一个变量和一个函数允许为空则必须声明可以为空  *？
    fun methodNull(str: Any): Int? {
        if (str !is String) return null
        return str.length
    }

    //安全调用
    fun methodNull(x: String) {
        print(x?.length) //如果 b 不为空则返回长度，否则返回空。这个表达式的的类型是 Int?

        /**
         * 如果 ?: 左边表达式不为空则返回，否则返回右边的表达式。注意右边的表带式只有在左边表达式为空是才会执行
         */
        val l = x.length ?: -1

        /**
         * !! 操作符  这会返回一个非空的 b 或者抛出一个 b 为空的 NPE
         */
        val le = x!!.length

        /**
         * 安全转换    普通的转换可能产生 ClassCastException 异常。另一个选择就是使用安全转换，如果不成功就返回空
         */
        val aInt: Int? = x as? Int
    }

    fun methodFor() {
        val items = listOf("1", "apple", "banana")
        for (item in items) {
            print(item)
        }

        for (index in items.indices) {
            print("$index 个 is ${items[index]}")
        }

        for (x in 1..10 step 2) {
            print(x)
        }


        /**
         * 使用lambda表达式过滤和映射集合 it 代表单个数据条
         */
        items
                .filter { it.startsWith("a") }
                .sortedBy { it }
                .map { it.toUpperCase() }
                .forEach { println(it) }
    }

    //数据类
    data class DataObject(val name: String,val email: String)


}

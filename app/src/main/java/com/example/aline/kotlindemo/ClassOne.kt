package com.example.aline.kotlindemo

/**
 * Created by liguoying on 2017/9/29.
 */
class ClassOne /*constructor*/(a: Int) {

//    class ClassOne private constructor() {私有化构造函数


    /**
     * lateinit 和 lazy 是 Kotlin 中的两种不同的延迟初始化技术。
     *lateinit 只用于 var，而 lazy 只用于 val
     *lazy 应用于单例模式(if-null-then-init-else-return)，而且当且仅当变量被第一次调用的时候，委托方法才会执行。
     *lateinit 则用于只能生命周期流程中进行获取或者初始化的变量，比如 Android 的 onCreate()
     */

    var count1: Int

    init {
        count1 = a * a
    }

    var b = a * 3;
    var count2 = count1
    lateinit var dataFromClassTwo: String

    val classTwo = ClassTwo("name")
    fun getFromClassOne() {
        classTwo.getSame()
        ClassTwo.getInstance().getInstance()
    }
}
package com.example.aline.kotlindemo.utils

import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import com.example.aline.kotlindemo.R
import kotlinx.android.synthetic.main.title_bar.view.*

/**
 * Created by liguoying on 2017/10/18.
 */
class TitleBar : LinearLayout {

    private var mContext: Context? = null

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        mContext = context
        if (isInEditMode) return
        val titlbar = LayoutInflater.from(context).inflate(R.layout.title_bar,
                ((getContext() as Activity).window) as ViewGroup, false)
        addView(titlbar, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        title_bar_left
    }

    fun getIvRight(): ImageView {
        return title_bar_right_icon
    }

    fun setTitleBarText(title: String) {
        title_bar_title.text = title
    }

    fun setTitleBarColor(color: Int) {
        title_bar_title.setTextColor(color)
    }

    fun setLeftDrawable(id: Int) {
        title_bar_left_icon.visibility = View.VISIBLE
        title_bar_left_text.visibility = View.GONE
        title_bar_left_icon.setImageDrawable(ContextCompat.getDrawable(mContext, id))
    }

    fun setLeftText(text: String) {
        title_bar_left_icon.visibility = View.GONE
        title_bar_left_text.visibility = View.VISIBLE
        title_bar_left_text.text = text
    }

    fun setLeftTextColor(color: Int) {
        title_bar_left_text.setTextColor(ContextCompat.getColor(mContext, color))
    }

    fun setRightTextColor(color: Int) {
        title_bar_right_text.setTextColor(ContextCompat.getColor(mContext, color))
    }

    fun setRightText(text: String) {
        title_bar_right_icon.visibility = View.GONE
        title_bar_right_text.visibility = View.VISIBLE
        title_bar_right_text.text = text
    }

    fun setRightDrawable(id: Int) {
        title_bar_right_icon.visibility = View.VISIBLE
        title_bar_right_text.visibility = View.GONE
        title_bar_right_icon.setImageDrawable(ContextCompat.getDrawable(context, id))
    }

    public fun setRightFirstDrawable(id: Int) {
        title_bar_right_first_icon.visibility = View.VISIBLE
        title_bar_right_first_icon.setImageDrawable(ContextCompat.getDrawable(mContext, id))
    }

    public fun setRightFirstVisiblity(visiblity: Int){
        title_bar_first_right.visibility = visibility
    }

    fun setRightVisibility(visiblity: Int) {
        togatherRight(visiblity == View.VISIBLE)
    }

    private fun togatherRight(status: Boolean) {
        when (status) {
            true -> title_bar_right.visibility = View.VISIBLE
            else -> title_bar_right.visibility = View.GONE
        }
    }

    private fun setLeftVisibility(visiblity: Int) {
        title_bar_left.visibility = visibility
    }

    public fun setLeftTitleBarClick(onClickListener: OnClickListener) {
        title_bar_left.setOnClickListener(onClickListener)
    }

    public fun setLeftTextClick(onClickListener: OnClickListener) {
        title_bar_left_text.setOnClickListener(onClickListener)
    }

    public fun setRightClick(onClickListener: OnClickListener) {
        title_bar_right.setOnClickListener(onClickListener)
    }

    public fun setRightTextClick(onClickListener: OnClickListener) {
        title_bar_right_text.setOnClickListener(onClickListener)
    }

    public fun setLeftFirstClick(onClickListener: OnClickListener) {
        title_bar_first_right.setOnClickListener(onClickListener)
    }


    public fun setLeftTextAndIcon(text: String,id: Int,isIconAtLeft: Boolean){
        title_bar_left_text.text = text
        title_bar_left_text.visibility = View.VISIBLE
        title_bar_left_icon.visibility = View.GONE
        when(isIconAtLeft){
            true -> title_bar_left_text.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(mContext,id),null,null,null)
            else -> title_bar_left_text.setCompoundDrawablesWithIntrinsicBounds(null,null,ContextCompat.getDrawable(mContext,id),null)
        }
        title_bar_right_text.compoundDrawablePadding = 10
    }

    public fun setRightAndIcon(text: String,id: Int,isIconAtRight: Boolean){
        title_bar_right_text.text = text
        title_bar_right_text.visibility = View.VISIBLE
        title_bar_right_icon.visibility = View.GONE
        when(isIconAtRight){
            true -> title_bar_right_text.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(mContext,id),null,null,null)
            else -> title_bar_right_text.setCompoundDrawablesWithIntrinsicBounds(null,null,ContextCompat.getDrawable(mContext,id),null)
        }
        title_bar_left_text.compoundDrawablePadding = 10
    }

}
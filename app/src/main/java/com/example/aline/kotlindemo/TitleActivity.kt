package com.example.aline.kotlindemo

import android.os.Bundle
import android.support.annotation.IntDef
import android.view.View
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Retention

class TitleActivity : BaseActivity() {
    @IntDef(View.VISIBLE.toLong(), View.INVISIBLE.toLong(), View.GONE.toLong())
    @Retention(RetentionPolicy.SOURCE)
    internal annotation class Visility

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_title2)
    }
}

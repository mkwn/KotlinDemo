package com.example.aline.kotlindemo

/**
 * Created by liguoying on 2017/9/29.
 */
class ClassTwo(count: String) {


    companion object {
        //伴生对象，相当于java中的静态方法
        fun getInstance() = this
    }

    lateinit var upper: String

    //    次构造器
    constructor(count1: String, num: Int) : this(count1) {
        upper = count1 + num
    }

    var upperNum = upper.toUpperCase()
     fun getSame(): String {
        return upper
    }

    fun getSomething(obj: Any): Int? {
        when (obj) {
            obj is Int -> return 1
            else -> "is not Integer"
        }
        return null
    }

    fun getSomethingelse(count: Int): Int =
            when (count) {
                1 -> 1
                2 -> 2
                else -> 3
            }
}

